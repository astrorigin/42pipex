/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   paths.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/18 13:47:49 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 00:04:24 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

char	**paths(char *env[])
{
	static char	**dirs;
	char		*path;

	if (!dirs)
	{
		path = ft_env_var("PATH", env);
		if (!path)
		{
			error("ft_env_var", "PATH not found");
			return (0);
		}
		dirs = ft_split(path, ':');
		if (!dirs)
		{
			error("ft_split", "nomem");
			return (0);
		}
	}
	return (dirs);
}
