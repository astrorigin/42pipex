/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/16 14:06:52 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 00:04:11 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static int	checkargs(int argc, char *argv[])
{
	int	i;

	if (argc < 5)
		return (error("pipex", "Missing argument"));
	i = 0;
	while (i < argc)
	{
		if (!argv[i] || !*argv[i] || ft_strisspace(argv[i]))
			return (error("pipex", "Empty argument"));
		++i;
	}
	return (1);
}

static int	proceed(t_fildes *fildes, int ncmd, char *cmds[], char *env[])
{
	int		i;
	pid_t	pid;

	if (!first_cmd(fildes, cmds[0], env))
		return (0);
	i = 1;
	while (i < ncmd - 1)
	{
		if (!second_cmd(fildes, (i * 2) - 2, cmds[i], env))
			return (0);
		++i;
	}
	pid = last_cmd(fildes, (i * 2) - 2, cmds[i], env);
	if (!pid)
		return (0);
	if (waitpid(pid, 0, 0) < 0)
		return (error("waitpid", "f4iled"));
	return (1);
}

int	main(int argc, char *argv[], char *env[])
{
	t_fildes	fildes;
	char		**args;

	if (!checkargs(argc, argv))
		return (1);
	if (!ft_strcmp(argv[1], "here_doc"))
	{
		if (!init_hd(&fildes, argv[2])
			|| !init_outfile(&fildes, argv[argc - 1]))
			return (1);
		argc -= 4;
		args = &argv[3];
	}
	else
	{
		if (!init_files(&fildes, argc, argv))
			return (1);
		argc -= 3;
		args = &argv[2];
	}
	if (!init_pipes(&fildes, argc) || !paths(env)
		|| !proceed(&fildes, argc, args, env))
		return (1);
	ft_memtrash();
	return (0);
}
