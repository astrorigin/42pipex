# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/12/16 14:01:10 by pmarquis          #+#    #+#              #
#    Updated: 2022/12/19 11:51:02 by pmarquis         ###   lausanne.ch        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re mrproper

# options

OPT_DEBUG = 0
export OPT_DEBUG

OPT_LINUX = 0
export OPT_LINUX

# end options

NAME = pipex

CC = gcc
ifeq ($(OPT_DEBUG),1)
CFLAGS = -Wall -Wextra -g -fsanitize=address #-Werror
else
CFLAGS = -Wall -Wextra -Werror
endif

DEFINES =

ifeq ($(OPT_DEBUG),0)
DEFINES += -DNDEBUG
endif

ifeq ($(OPT_LINUX),1)
DEFINES += -DLINUX
endif

INCLUDES = -Ilibft -Ilibft/gnl

INC = pipex.h
SRC = error.c firstcmd.c init.c lastcmd.c main.c paths.c secondcmd.c
OBJ = $(patsubst %.c,%.o,$(SRC))

ARCHIVES = libft/gnl/libftgnl.a libft/libft.a

.c.o:
	$(CC) -c $(CFLAGS) $(DEFINES) $(INCLUDES) $< -o $@

.DEFAULT_GOAL = all

all: $(NAME)

$(NAME): $(OBJ) $(ARCHIVES)
	$(CC) $(CFLAGS) $^ -o $@

libft/libft.a:
	$(MAKE) -C libft

libft/gnl/libftgnl.a:
	$(MAKE) -C libft/gnl

clean:
	rm -f *.o
	$(MAKE) -C libft $@

fclean: clean
	rm -f $(NAME)
	$(MAKE) -C libft $@

re: fclean all

mrproper: fclean
	rm -f cscope.* tags
	$(MAKE) -C libft $@

### deps
$(OBJ): $(INC)

