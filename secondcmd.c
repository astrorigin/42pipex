/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   secondcmd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/18 16:48:49 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 00:05:18 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

pid_t	second_cmd(t_fildes *fildes, int i, const char *cmd, char *env[])
{
	pid_t	pid;
	char	**args;

	pid = fork();
	if (pid == 0)
	{
		if (dup2(fildes->pipes[i], 0) < 0 || dup2(fildes->pipes[i + 3], 1) < 0)
			return (error("dup2", strerror(errno)));
		if (!close_fildes(fildes))
			return (0);
		args = ft_split(cmd, ' ');
		if (!args)
			return (error("ft_split", "nomem"));
		execve(ft_abspath_of_cmd(args[0], paths(env)), args, env);
		return (error(args[0], strerror(errno)));
	}
	return (pid);
}
