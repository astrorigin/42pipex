/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/16 14:07:55 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 01:49:56 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include <errno.h>
# include <string.h>
# include <sys/wait.h>

# ifndef NDEBUG
#  include <stdio.h>
# endif

# include <libft.h>
# include <ft_getnext.h>

typedef struct s_fildes
{
	int		infile_fd;
	int		outfile_fd;
	int		*pipes;
	int		n;
	int		hd[2];
}	t_fildes;

int		error(const char *title, const char *msg);
int		compare_hd(const char *line, const char *hd);
char	**paths(char *env[]);

int		init_files(t_fildes *fildes, int argc, char *argv[]);
int		init_hd(t_fildes *fildes, const char *hd);
int		init_outfile(t_fildes *fildes, const char *path);
int		init_pipes(t_fildes *fildes, int ncmd);
int		close_fildes(t_fildes *fildes);

pid_t	first_cmd(t_fildes *fildes, const char *cmd, char *env[]);
pid_t	second_cmd(t_fildes *fildes, int i, const char *cmd, char *env[]);
pid_t	last_cmd(t_fildes *fildes, int i, const char *cmd, char *env[]);

#endif
