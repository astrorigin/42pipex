/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/18 02:07:41 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 00:02:13 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	error(const char *title, const char *msg)
{
	ft_putstr("Error: ", 2);
	ft_putstr(title, 2);
	ft_putstr(": ", 2);
	ft_putstr(msg, 2);
	ft_putchar('\n', 2);
	ft_memtrash();
	return (0);
}

int	compare_hd(const char *line, const char *hd)
{
	size_t	s1;
	size_t	s2;

	if (!ft_startswith(line, hd))
		return (0);
	if (ft_strlen2(line, &s1) == ft_strlen2(hd, &s2))
		return (1);
	if (s1 == s2 + 1 && ft_endswith(line, "\n"))
		return (1);
	return (0);
}
