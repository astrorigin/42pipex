/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   firstcmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/18 13:46:17 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 00:04:53 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

pid_t	first_cmd(t_fildes *fildes, const char *cmd, char *env[])
{
	pid_t	pid;
	char	**args;

	pid = fork();
	if (pid == 0)
	{
		if (fildes->infile_fd != -1)
		{
			if (dup2(fildes->infile_fd, 0) < 0 || dup2(fildes->pipes[1], 1) < 0)
				return (error("dup2", strerror(errno)));
		}
		else
		{
			if (dup2(fildes->hd[0], 0) < 0 || dup2(fildes->pipes[1], 1) < 0)
				return (error("dup2", strerror(errno)));
		}
		if (!close_fildes(fildes))
			return (0);
		args = ft_split(cmd, ' ');
		if (!args)
			return (error("ft_split", "nomem"));
		execve(ft_abspath_of_cmd(args[0], paths(env)), args, env);
		return (error(args[0], strerror(errno)));
	}
	return (pid);
}
