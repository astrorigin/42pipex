/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/14 00:34:45 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/16 20:27:17 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_del(void *p)
{
	char	**sp;

	sp = (char **) p;
	if (*sp)
	{
		ft_free(*sp);
		*sp = 0;
	}
}

void	ft_del_arr(void *p)
{
	char	***arr;
	size_t	i;

	arr = (char ***) p;
	if (*arr)
	{
		i = 0;
		while (1)
		{
			if ((*arr)[i])
				ft_free((*arr)[i++]);
			else
				break ;
		}
		ft_free(*arr);
		*arr = 0;
	}
}
