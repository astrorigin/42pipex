/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_chrstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/07 23:31:20 by pmarquis          #+#    #+#             */
/*   Updated: 2022/11/20 16:04:50 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_chrstr(char c, size_t n, char **ret)
{
	register size_t	i;
	char			*p;

	p = ft_malloc(sizeof(char) * (n + 1));
	if (!p)
		return (0);
	*ret = p;
	i = 0;
	while (i++ < n)
		*p++ = c;
	*p = 0;
	return (1);
}
