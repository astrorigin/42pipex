/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/30 18:40:36 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/18 23:39:34 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isspace(int c)
{
	if (!ft_strchr(" \n\t\r\f\v", c))
		return (0);
	return (1);
}

int	ft_strisspace(const char *s)
{
	while (*s)
	{
		if (!ft_isspace(*s++))
			return (0);
	}
	return (1);
}
