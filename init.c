/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/18 11:38:56 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 11:42:50 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	init_hd(t_fildes *fildes, const char *hd)
{
	char	*line;

	fildes->infile_fd = -1;
	if (pipe(fildes->hd) < 0)
		return (error("pipe hd", strerror(errno)));
	line = ft_getnextline(0);
	while (line)
	{
		if (compare_hd(line, hd))
		{
			ft_free(line);
			break ;
		}
		if (!ft_putstr(line, fildes->hd[1]))
			return (error("ft_putstr", strerror(errno)));
		ft_free(line);
		line = ft_getnextline(0);
	}
	if (fildes->hd[1] != -1)
	{
		if (close(fildes->hd[1]) < 0)
			return (error("close hd 1", strerror(errno)));
		fildes->hd[1] = -1;
	}
	return (1);
}

int	init_outfile(t_fildes *fildes, const char *path)
{
	if (fildes->infile_fd != -1)
	{
		fildes->outfile_fd = open(path,
				O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	}
	else
	{
		fildes->outfile_fd = open(path,
				O_CREAT | O_WRONLY | O_APPEND,
				S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	}
	if (fildes->outfile_fd < 0)
		return (error(path, strerror(errno)));
	return (1);
}

int	init_files(t_fildes *fildes, int argc, char *argv[])
{
	fildes->infile_fd = open(argv[1], O_RDONLY);
	if (fildes->infile_fd < 0)
		return (error(argv[1], strerror(errno)));
	if (!init_outfile(fildes, argv[argc - 1]))
		return (0);
	fildes->hd[0] = -1;
	fildes->hd[1] = -1;
	return (1);
}

int	init_pipes(t_fildes *fildes, int ncmd)
{
	int	i;

	fildes->pipes = ft_calloc(2 * (ncmd - 1), sizeof(int));
	if (!fildes->pipes)
		return (error("init_pipes", "nomem"));
	i = 0;
	while (i < ncmd - 1)
	{
		if (pipe(&fildes->pipes[i * 2]) < 0)
			return (error("pipe", strerror(errno)));
		++i;
	}
	fildes->n = 2 * (ncmd - 1);
	return (1);
}

int	close_fildes(t_fildes *fildes)
{
	int	i;

	if ((fildes->infile_fd != -1 && close(fildes->infile_fd) < 0)
		|| close(fildes->outfile_fd) < 0)
		return (error("close", strerror(errno)));
	i = 0;
	while (i < fildes->n)
	{
		if (close(fildes->pipes[i++]) < 0)
			return (error("close", strerror(errno)));
	}
	ft_free(fildes->pipes);
	if (fildes->hd[0] != -1)
	{
		if (close(fildes->hd[0]) < 0)
			return (error("close", strerror(errno)));
		fildes->hd[0] = -1;
	}
	return (1);
}
